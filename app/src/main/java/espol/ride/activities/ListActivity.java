package espol.ride.activities;


import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import espol.ride.R;
import espol.ride.adapters.RecyclerAdapter;
import espol.ride.databinding.ActivityListBinding;
import espol.ride.models.Route;
import espol.ride.models.Vehicle;
import espol.ride.utils.APIRide;
import espol.ride.utils.Utils;


public class ListActivity extends AppCompatActivity implements RecyclerAdapter.OnItemClickListener {

    private final String TAG = ListActivity.class.getSimpleName();

    private Context mContext = this;

    private ArrayList mDataSet;

    private ActivityListBinding mBinding;

    private RecyclerAdapter mRecyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(ListActivity.this,
                R.layout.activity_list);

        // SUPPORT ACTION BAR
        Utils.setToolbar(mContext, mBinding.toolbar);

        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recyclerView.setHasFixedSize(true);

        mBinding.swipeRefreshList.setColorSchemeResources(R.color.colorAccent);
        mBinding.swipeRefreshList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getList(getIntent().getStringExtra(Utils.GO_TO));
            }
        });

        setFabAddAction(getIntent().getStringExtra(Utils.GO_TO));
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBinding.swipeRefreshList.setRefreshing(true);
        getList(getIntent().getStringExtra(Utils.GO_TO));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setFabAddAction(String goTo) {
        switch (goTo) {
            case Utils.VEHICLES:
                setCreateClass(VehicleFormActivity.class);
                break;
            case Utils.ROUTES:
                setCreateClass(RouteFormActivity.class);
                break;
            default:
                ((View) mBinding.fabAdd).setVisibility(View.GONE);
                break;
        }
    }

    private void setCreateClass(final Class classToGo) {
        mBinding.fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, classToGo));
            }
        });
    }

    private void setRetrieveClass(Class classToGo, Object o) {
        Intent intent = new Intent(mContext, classToGo);
        intent.putExtra("OBJECT", new Gson().toJson(o));
        intent.putExtra("ACTION", "UPDATE");
        startActivity(intent);
    }

    private void getList(String goTo) {
        String title = getResources().getString(R.string.vehicles);
        switch (goTo) {
            case Utils.VEHICLES:
                title = getResources().getString(R.string.vehicles);
                getVehicleList();
                break;
            case Utils.ROUTES:
                title = getResources().getString(R.string.routes);
                getRouteList();
                break;
        }
        mBinding.listTitle.setText(title);
    }


    private void getVehicleList() {
        APIRide.get(APIRide.VEHICLES, null,
                new Utils.CustomJsonHttpResponseHandler(mContext) {
            @Override
            public void onStart() {
                mBinding.swipeRefreshList.setRefreshing(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                mBinding.swipeRefreshList.setRefreshing(false);

                Type type = new TypeToken<ArrayList<Vehicle>>() {
                }.getType();
                mDataSet = new Gson().fromJson(response.toString(), type);
                //noinspection unchecked
                mRecyclerAdapter = new RecyclerAdapter<Vehicle>(mDataSet, R.layout.item_vehicle,
                        ListActivity.this);
                mBinding.recyclerView.setAdapter(mRecyclerAdapter);
                mBinding.swipeRefreshList.setRefreshing(false);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                                  JSONObject errorResponse) {
                mBinding.swipeRefreshList.setRefreshing(false);
                Log.i("ERROR V", throwable.getMessage());
                Utils.shortToast(mContext, "ERROR");
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }
        });
    }

    private void getRouteList() {
        APIRide.get(APIRide.ROUTES, null, new Utils.CustomJsonHttpResponseHandler(mContext) {
            @Override
            public void onStart() {
                mBinding.swipeRefreshList.setRefreshing(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                mBinding.swipeRefreshList.setRefreshing(false);

                Type type = new TypeToken<ArrayList<Route>>() {}.getType();
                mDataSet = new Gson().fromJson(response.toString(), type);
                //noinspection unchecked
                mRecyclerAdapter = new RecyclerAdapter<Route>(mDataSet, R.layout.item_route,
                        ListActivity.this);
                mBinding.recyclerView.setAdapter(mRecyclerAdapter);
                mBinding.swipeRefreshList.setRefreshing(false);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                                  JSONObject errorResponse) {
                mBinding.swipeRefreshList.setRefreshing(false);
                Utils.shortToast(mContext, errorResponse.toString());
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }
        });
    }


    @Override
    public boolean onItemClick(Object item) {

        if (item instanceof Vehicle) {

            final Vehicle vehicle = (Vehicle) item;
            APIRide.get(String.format(Locale.getDefault(), "%s%d/",
                    APIRide.VEHICLES, vehicle.getId()), null,
                    new Utils.CustomJsonHttpResponseHandler(mContext) {
                @Override
                public void onStart() {
                    mBinding.swipeRefreshList.setRefreshing(true);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    mBinding.swipeRefreshList.setRefreshing(false);
                    Vehicle vehicle1 = new Gson().fromJson(response.toString(), Vehicle.class);
                    setRetrieveClass(VehicleFormActivity.class, vehicle1);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                                      JSONObject errorResponse) {
                    mBinding.swipeRefreshList.setRefreshing(false);
                    Utils.shortToast(mContext, errorResponse.toString());
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }
            });

        } else if (item instanceof Route) {

            final Route route = (Route) item;
            APIRide.get(String.format(Locale.getDefault(), "%s%d/",
                    APIRide.ROUTES, route.getId()), null,
                    new Utils.CustomJsonHttpResponseHandler(mContext) {
                @Override
                public void onStart() {
                    mBinding.swipeRefreshList.setRefreshing(true);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    mBinding.swipeRefreshList.setRefreshing(false);
                    Route route1 = new Gson().fromJson(response.toString(), Route.class);
                    setRetrieveClass(RouteFormActivity.class, route1);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                                      JSONObject errorResponse) {
                    mBinding.swipeRefreshList.setRefreshing(false);
                    Utils.shortToast(mContext, errorResponse.toString());
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }
            });

        }

        return false;
    }
}
