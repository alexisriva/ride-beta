package espol.ride.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONObject;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.kxml2.kdom.Element;

import java.io.File;
import java.io.FileOutputStream;

import cz.msebera.android.httpclient.Header;
import espol.ride.R;
import espol.ride.databinding.ActivityLoginBinding;
import espol.ride.models.Profile;
import espol.ride.models.Student;
import espol.ride.models.UserProfile;
import espol.ride.utils.APIRide;
import espol.ride.utils.SessionManager;
import espol.ride.utils.Utils;
import espol.ride.utils.WSMedia;
import espol.ride.utils.WSRide;
import espol.ride.utils.WSRideAuth;
import espol.ride.utils.WSRideInfo;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class LoginActivity extends AppCompatActivity {

    private Context mContext = this;

    private ActivityLoginBinding mBinding;

    private SessionManager mSessionManager;

    private Student mStudent;
    private File mProfileImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        mSessionManager = new SessionManager(mContext);

        mBinding.login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin();
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    public void attemptLogin() {
        String username = mBinding.loginUsername.getText().toString();
        String password = mBinding.loginPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(password)) {
            mBinding.loginPassword.setError(getString(R.string.field_required));
            focusView = mBinding.loginPassword;
            cancel = true;
        }

        if (TextUtils.isEmpty(username)) {
            mBinding.loginUsername.setError(getString(R.string.field_required));
            focusView = mBinding.loginUsername;
            cancel = true;
        }

        if (cancel)
            focusView.requestFocus();
        else {
            new AuthenticationLogin().execute(mBinding.loginUsername.getText().toString(),
                    mBinding.loginPassword.getText().toString());
        }
    }

    private void checkUser() {
        String url = APIRide.CHECK_USER + mBinding.loginUsername.getText().toString();

        APIRide.get(url, null, new TextHttpResponseHandler() {

            @Override
            public void onStart() {
                Utils.showProgressDialog(mContext, "Checking user");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Utils.showErrorDialog(mContext, responseString);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                if (Boolean.valueOf(responseString)) {
                    getUserToken();
                } else {
                    new ImageLoader().execute(mStudent.getUserId());
//                    new StudentInfo().execute(mBinding.loginUsername.getText().toString());
                }
            }
        });
    }

    private void registerUser(final UserProfile userProfile) {
        try {
            String url = APIRide.NEW_USER;

            RequestParams params = new RequestParams();
            params.put("username", userProfile.getUsername());
            params.put("email", userProfile.getEmail());
            params.put("password", userProfile.getPassword());
            params.put("first_name", userProfile.getFirst_name());
            params.put("last_name", userProfile.getLast_name());
            params.put("id", userProfile.getId());
            params.put("student_id", userProfile.getStudent_id());
            params.put("faculty", userProfile.getFaculty());
            params.put("photo", mProfileImage);

            APIRide.post(url, params, new Utils.CustomJsonHttpResponseHandler(mContext){
                @Override
                public void onStart() {
                    Utils.showProgressDialog(mContext, "Registering user");
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                    try {
                        if (response.getBoolean("result")) {
                            mProfileImage.delete();
                            getUserToken();
                        }
                        else
                            Utils.showErrorDialog(mContext, "User couldn't be registered");
                    } catch (Exception e) {
                        Utils.showErrorDialog(mContext, e.getMessage());
                    }
                }
            });
        } catch (Exception ex) {
            Utils.showErrorDialog(mContext, ex.getMessage());
        }
    }

    private void getUserToken() {
        String url = APIRide.TOKEN_AUTH;

        RequestParams params = new RequestParams();
        params.put("username", mBinding.loginUsername.getText().toString());
        params.put("password", mBinding.loginPassword.getText().toString());

        APIRide.post(url, params, new Utils.CustomJsonHttpResponseHandler(mContext){
            @Override
            public void onStart() {
                Utils.showProgressDialog(mContext, "Getting token");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    mSessionManager.setToken(response.getString("token"));
                    getProfile();
                } catch (Exception e) {
                    Utils.showErrorDialog(mContext, e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (statusCode == 400) {
                    changeUserPassword();
                } else {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }
            }
        });
    }

    private void changeUserPassword() {
        String url = APIRide.CHANGEPASSWORD;

        RequestParams requestParams = new RequestParams();
        requestParams.put("username", mBinding.loginUsername.getText().toString());
        requestParams.put("new_password", mBinding.loginPassword.getText().toString());

        APIRide.put(url, requestParams, new Utils.CustomJsonHttpResponseHandler(mContext) {
            @Override
            public void onStart() {
                Utils.showProgressDialog(mContext, "Actualizando credenciales");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                if (responseString.equalsIgnoreCase("success.")) {
                    getUserToken();
                }
            }
        });
    }

    private void getProfile() {
        String url = APIRide.PROFILES + mStudent.getUserId() + "/";

        APIRide.addAuth(mSessionManager.getToken());

        APIRide.get(url, null, new Utils.CustomJsonHttpResponseHandler(mContext){
            @Override
            public void onStart() {
                Utils.showProgressDialog(mContext, "Getting profile");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Utils.hideProgressDialog();

                try {
                    Profile profile = new Gson().fromJson(response.toString(),
                            Profile.class);
                    mStudent.setPhoto(profile.getPhoto());
                    mSessionManager.setStudent(new Gson().toJson(mStudent));
                    mSessionManager.setLogedIn(true);
                    mSessionManager.setRole(Utils.RIDER);
                    Intent intent = new Intent(mContext, MainBaseActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
//                    String token = FirebaseInstanceId.getInstance().getId();
//                    RequestParams requestParams = new RequestParams();
//                    requestParams.add("registration_id", token);
//                    requestParams.add("type", "android");
//                    APIRide.post(APIRide.DEVICES, requestParams,
//                            new Utils.CustomJsonHttpResponseHandler(mContext) {});

                } catch (Exception e) {
                    Utils.showErrorDialog(mContext, e.getMessage());
                }
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private class AuthenticationLogin extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.showProgressDialog(mContext, getString(R.string.auth_prompt));
        }

        @Override
        protected void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
            if (bool != null) {
                if (bool) {
                    new StudentInfo().execute(mBinding.loginUsername.getText().toString());
//                    checkUser();
                } else {
                    Utils.showErrorDialog(mContext, getString(R.string.login_error_prompt));
                }
            }
        }

        protected Boolean doInBackground(String... params) {
            HttpTransportSE httpTransport = new HttpTransportSE(WSRide.URL);
            SoapObject request = new SoapObject(WSRide.NAMESPACE, WSRideAuth.METHOD_NAME);
            request.addProperty(WSRideAuth.PROPERTY0, params[0]);
            request.addProperty(WSRideAuth.PROPERTY1, params[1]);
            SoapSerializationEnvelope envelope =
                    new SoapSerializationEnvelope(SoapSerializationEnvelope.VER11);
            envelope.headerOut = new Element[] {WSRide.buildAuthHeader()};
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            SoapObject bodyIn;
            try {
                httpTransport.call(WSRideAuth.SOAP_ACTION, envelope);
                bodyIn = (SoapObject) envelope.bodyIn;
                return Boolean.valueOf(bodyIn.getProperty(0).toString() );
            } catch (Exception e) {
                final Exception ex = e;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() { Utils.showErrorDialog(mContext, ex.getMessage()); }
                });
                return null;
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class StudentInfo extends AsyncTask<String, Void, Student> {

        @Override
        protected Student doInBackground(String... params) {
            HttpTransportSE httpTransport = new HttpTransportSE(WSRide.URL);
            SoapObject request = new SoapObject(WSRide.NAMESPACE, WSRideInfo.METHOD_NAME);
            request.addProperty(WSRideInfo.PROPERTY0, params[0]);
            SoapSerializationEnvelope envelope =
                    new SoapSerializationEnvelope(SoapSerializationEnvelope.VER11);
            envelope.headerOut = new Element[] { WSRide.buildAuthHeader() };
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            SoapObject bodyIn;
            try {
                httpTransport.call(WSRideInfo.SOAP_ACTION, envelope);
                bodyIn = (SoapObject) envelope.bodyIn;
                bodyIn = (SoapObject) bodyIn.getProperty("wsInfoEstudianteGeneralResult");
                bodyIn = (SoapObject) bodyIn.getProperty("diffgram");
                bodyIn = (SoapObject) bodyIn.getProperty("NewDataSet");
                bodyIn = (SoapObject) bodyIn.getProperty("ESTUDIANTE");
                String[] data = new String[7];
                data[0] = bodyIn.getProperty("USUARIO").toString();
                data[1] = bodyIn.getProperty("MATRICULA").toString();
                data[2] = bodyIn.getProperty("IDENTIFICACION").toString();
                data[3] = bodyIn.getProperty("NOMBRES").toString();
                data[4] = bodyIn.getProperty("APELLIDOS").toString();
                data[5] = bodyIn.getProperty("FACULTAD").toString();
                data[6] = bodyIn.getProperty("FOTO").toString();
                mStudent = new Student(data);
                return mStudent;
            } catch (Exception e) {
                final Exception ex = e;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() { Utils.showErrorDialog(mContext, ex.getMessage()); }
                });
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            String message = String.format("%s %s", "getting", "data");
            Utils.showProgressDialog(mContext, message);
        }

        @Override
        protected void onPostExecute(Student result) {
            super.onPostExecute(result);
            if (result != null) {
//                new ImageLoader().execute(result.getUserId());
                checkUser();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class ImageLoader extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.showProgressDialog(mContext, "Getting image");
        }

        @Override
        protected void onPostExecute(String img) {
            super.onPostExecute(img);
            try {
                byte[] photoAsBytes = Base64.decode(img, 0);
                mProfileImage = new File(mContext.getFilesDir(), "profile.jpg");
                FileOutputStream os = new FileOutputStream(mProfileImage, false);
                os.write(photoAsBytes);
                os.flush();
                os.close();
                UserProfile userProfile = new UserProfile(
                        mBinding.loginUsername.getText().toString(),
                        mBinding.loginUsername.getText().toString() + "@espol.edu.ec",
                        mBinding.loginPassword.getText().toString(),
                        mStudent.getFirstName(),
                        mStudent.getLastName(),
                        mStudent.getId(),
                        mStudent.getUserId(),
                        mStudent.getSchool()
                );
                registerUser(userProfile);
            } catch (Exception e) {
                Utils.showErrorDialog(mContext, e.getMessage());
            }
        }

        protected String doInBackground(String... params) {
            HttpTransportSE httpTransport = new HttpTransportSE(WSMedia.URL);
            SoapObject request = new SoapObject(WSMedia.NAMESPACE, WSMedia.METHOD_NAME);
            request.addProperty(WSMedia.PROPERTY0, params[0]);
            SoapSerializationEnvelope envelope =
                    new SoapSerializationEnvelope(SoapSerializationEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            SoapObject bodyIn;
            try {
                httpTransport.call(WSMedia.SOAP_ACTION,envelope);
                bodyIn = (SoapObject) envelope.bodyIn;
                return bodyIn.getProperty(0).toString();
            } catch (Exception e) {
                final Exception ex = e;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Utils.showErrorDialog(mContext, ex.getMessage());
                    }
                });
                return null;
            }
        }
    }
}
