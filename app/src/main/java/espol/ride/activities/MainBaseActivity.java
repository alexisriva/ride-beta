package espol.ride.activities;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.MenuItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import espol.ride.R;
import espol.ride.databinding.ActivityMainBaseBinding;
import espol.ride.fragments.DriverTripViewFragment;
import espol.ride.fragments.PostTripFragment;
import espol.ride.fragments.ProfileFragment;
import espol.ride.fragments.RiderTripViewFragment;
import espol.ride.fragments.FindTripFragment;
import espol.ride.utils.APIRide;
import espol.ride.utils.SessionManager;
import espol.ride.utils.Utils;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class MainBaseActivity extends AppCompatActivity {

    private final Context mContext = this;

    private static final int TRIPS = 0;
    private static final int PROFILE = 1;

    private class MainPagerAdapter extends FragmentPagerAdapter {
        private FragmentManager mManager;
        private final ArrayList<Fragment> mFragments = new ArrayList<>();

        private MainPagerAdapter(FragmentManager fm) {
            super(fm);
            mManager = fm;
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        private void addFragment(Fragment fragment) {
            mFragments.add(fragment);
        }

        private void reset() {
            for (int i = 0; i < mFragments.size(); i++)
                mManager.beginTransaction().remove(mFragments.get(1)).commit();
            mFragments.clear();
            notifyDataSetChanged();
        }

        private void replaceFragments(ArrayList<Fragment> fragments) {
            if (mFragments.size() > 0)
                reset();

            mFragments.addAll(fragments);
            notifyDataSetChanged();
        }
    }

    private ActivityMainBaseBinding mBinding;

    private MenuItem mPrevMenuItem;
    private int mCurrentSelectedItemId;

    private MainBaseActivity.MainPagerAdapter mAdapter;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            int itemId = menuItem.getItemId();
            mCurrentSelectedItemId = itemId;

            switch (itemId) {
                case R.id.navigation_trip:
                    mBinding.navViewPager.setCurrentItem(TRIPS);
                    return true;
                case R.id.navigation_profile:
                    mBinding.navViewPager.setCurrentItem(PROFILE);
                    return true;
            }

            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Adds authorization to requests
        APIRide.addAuth(new SessionManager(mContext).getToken());

        mBinding = DataBindingUtil
                .setContentView(MainBaseActivity.this, R.layout.activity_main_base);

        mBinding.bottomNav.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        mBinding.bottomNav.setSelectedItemId(R.id.navigation_trip);

        mBinding.navViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int pos, float posOffset, int posOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                if (mPrevMenuItem != null)
                    mPrevMenuItem.setChecked(false);
                else
                    mBinding.bottomNav.getMenu().getItem(0).setChecked(false);

                mBinding.bottomNav.getMenu().getItem(position).setChecked(true);
                mPrevMenuItem = mBinding.bottomNav.getMenu().getItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBinding.bottomNav.setSelectedItemId(mCurrentSelectedItemId);
        Utils.checkGpsEnabled(this, mBinding.getRoot());

        SessionManager sm = new SessionManager(mContext);
        if (sm.getRole().equals(Utils.RIDER)) {
            isRegisteredToTrip();
        } else {
            hasActiveTrips();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    public void setPagerAdapter(boolean rider, boolean registered) {
        if (mAdapter == null)
            mAdapter = new MainBaseActivity.MainPagerAdapter(getSupportFragmentManager());

        if (rider) {
            if (registered) {
                mAdapter.addFragment(new RiderTripViewFragment());
                mAdapter.addFragment(new ProfileFragment());
            } else {
                mAdapter.addFragment(new FindTripFragment());
                mAdapter.addFragment(new ProfileFragment());
            }
        } else {
            if (registered) {
                mAdapter.addFragment(new DriverTripViewFragment());
                mAdapter.addFragment(new ProfileFragment());
            } else {
                mAdapter.addFragment(new PostTripFragment());
                mAdapter.addFragment(new ProfileFragment());
            }
        }

        if (mBinding.navViewPager.getAdapter() == null)
            mBinding.navViewPager.setAdapter(mAdapter);
        else
            mAdapter.notifyDataSetChanged();
    }

    private void isRegisteredToTrip() {
        APIRide.get(APIRide.ISREGISTERED, null,
                new Utils.CustomJsonHttpResponseHandler(mContext) {
            @Override
            public void onStart() {
                Utils.showProgressDialog(mContext, "We're setting up");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Utils.hideProgressDialog();

                try {
                    boolean isRegistered = (boolean) response.get("result");
                    Log.i("REGISTERED", isRegistered + "");

                    setPagerAdapter(true, isRegistered);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Utils.hideProgressDialog();
                Utils.shortToast(mContext, "ERROR");
            }
        });
    }

    private void hasActiveTrips() {
        APIRide.get(APIRide.HASACTIVE, null,
                new Utils.CustomJsonHttpResponseHandler(mContext) {
            @Override
            public void onStart() {
                Utils.showProgressDialog(mContext, "We're setting up");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Utils.hideProgressDialog();

                try {
                    boolean hasActive = (boolean) response.get("result");
                    Log.i("ACTIVE", hasActive + "");

                    setPagerAdapter(false, hasActive);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Utils.hideProgressDialog();
                Utils.shortToast(mContext, "ERROR");
            }
        });
    }

}
