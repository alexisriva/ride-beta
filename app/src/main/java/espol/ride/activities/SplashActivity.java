package espol.ride.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import espol.ride.R;
import espol.ride.utils.APIRide;
import espol.ride.utils.SessionManager;
import espol.ride.utils.Utils;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class SplashActivity extends AppCompatActivity {

    Context mContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    private void tokenVerifyTask() {
        SessionManager sessionManager = new SessionManager(mContext);

        RequestParams requestParams = new RequestParams();
        requestParams.put("token", sessionManager.getToken());

        APIRide.post(APIRide.TOKEN_VERIFY, requestParams,
                new Utils.CustomJsonHttpResponseHandler(mContext){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                startActivity(new Intent(mContext, MainBaseActivity.class));
                finish();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                                  JSONObject errorResponse) {
                startActivity(new Intent(mContext, LoginActivity.class));
                finish();
            }
        });
    }

    private void init() {
        if (new SessionManager(mContext).getLogedIn())
            tokenVerifyTask();
        else {
            startActivity(new Intent(mContext, LoginActivity.class));
            finish();
        }
    }
}
