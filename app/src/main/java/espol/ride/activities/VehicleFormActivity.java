package espol.ride.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;

import com.google.gson.Gson;
import com.jaredrummler.android.colorpicker.ColorPickerDialog;
import com.jaredrummler.android.colorpicker.ColorPickerDialogListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import cz.msebera.android.httpclient.Header;
import espol.ride.R;
import espol.ride.databinding.ActivityVehicleFormBinding;
import espol.ride.models.Vehicle;
import espol.ride.utils.APIRide;
import espol.ride.utils.SessionManager;
import espol.ride.utils.Utils;
import espol.ride.utils.Validator;

public class VehicleFormActivity extends AppCompatActivity implements ColorPickerDialogListener {

    private final int CAR_PHOTO = 1;
    private final int LICENSE_PHOTO = 2;
    private final int PERMIT_PHOTO = 3;

    private Context mContext = this;

    private SessionManager mSessionManager;

    private ActivityVehicleFormBinding mBinding;

    private Vehicle mVehicle;
    private String mAction;

//    private List<Brand> mVehicleBrands = new ArrayList<>();
//    private List<Model> mVehicleModels = new ArrayList<>();

    private boolean isFinish = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(VehicleFormActivity.this,
                R.layout.activity_vehicle_form);

        mSessionManager = new SessionManager(mContext);

        String object = getIntent().getStringExtra("OBJECT");
        String toDo = getIntent().getStringExtra("ACTION");

        if (Validator.isValidString(object))
            mVehicle = new Gson().fromJson(object, Vehicle.class);
        else
            mVehicle = new Vehicle();

        if (Validator.isValidString(toDo))
            mAction = toDo;
        else
            mAction = "";

        mBinding.ivVehiclePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choosePhoto(v);
            }
        });
        mBinding.btnVehiclePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choosePhoto(v);
            }
        });

        mBinding.ivRegisterPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choosePhoto(v);
            }
        });
        mBinding.ivRegisterPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choosePhoto(v);
            }
        });

        mBinding.ivPermitPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choosePhoto(v);
            }
        });
        mBinding.btnPermitPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choosePhoto(v);
            }
        });

        mBinding.etColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseColor(v);
            }
        });

        String[] brands = getResources().getStringArray(R.array.brands);
        ArrayAdapter<String> brandsAdapter  =
                new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, brands);
        mBinding.etBrand.setThreshold(0);
        mBinding.etBrand.setAdapter(brandsAdapter);
        mBinding.etBrand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mBinding.etBrand.setText(null);
                mBinding.etBrand.showDropDown();
            }
        });
//        mBinding.etBrand.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    mBinding.etBrand.showDropDown();
//                }
//            }
//        });


        String[] models = getResources().getStringArray(R.array.models);
        ArrayAdapter<String> modelsAdapter =
                new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, models);
        mBinding.etModel.setAdapter(modelsAdapter);
        mBinding.etModel.setThreshold(0);
        mBinding.etModel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.etModel.showDropDown();
            }
        });
        mBinding.etModel.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    mBinding.etModel.showDropDown();
                }
            }
        });

//        getBrandsTask();
//        getModelsTask();

        init();
    }

    private void getBrandsTask() {
        APIRide.get(APIRide.BRANDS, null , new Utils.CustomJsonHttpResponseHandler(mContext) {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                String[] brands = getResources().getStringArray(R.array.brands);
                ArrayAdapter<String> brandsAdapter  =
                        new ArrayAdapter<>(getApplicationContext(),
                                android.R.layout.simple_list_item_1, brands);
                mBinding.etBrand.setAdapter(brandsAdapter);
            }
        });
    }

    private void getModelsTask(int brandId) {
        APIRide.get(APIRide.BRANDS+"/"+brandId+"/", null , new Utils.CustomJsonHttpResponseHandler(mContext) {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                String[] models = getResources().getStringArray(R.array.models);
                ArrayAdapter<String> modelsAdapter =
                        new ArrayAdapter<>(getApplicationContext(),
                                android.R.layout.simple_list_item_1, models);
                mBinding.etModel.setAdapter(modelsAdapter);
            }
        });
    }

    private void init() {
        if (mAction.equals("UPDATE")) {
            isFinish = true;
            Log.i("EQUIP UPD", mAction);

            if (mVehicle.getPhoto() != null) {
                Picasso.get().load(mVehicle.getPhoto()).into(mBinding.ivVehiclePhoto);
                mBinding.ivVehiclePhoto.setVisibility(View.VISIBLE);
                mBinding.btnVehiclePhoto.setVisibility(View.GONE);
            }

            if (mVehicle.getRegisterPhoto() != null) {
                Picasso.get().load(mVehicle.getRegisterPhoto()).into(mBinding.ivRegisterPhoto);
                mBinding.ivRegisterPhoto.setVisibility(View.VISIBLE);
                mBinding.btnRegisterPhoto.setVisibility(View.GONE);
            }

            if (mVehicle.getPermitPhoto() != null) {
                Picasso.get().load(mVehicle.getPhoto()).into(mBinding.ivPermitPhoto);
                mBinding.ivPermitPhoto.setVisibility(View.VISIBLE);
                mBinding.btnPermitPhoto.setVisibility(View.GONE);
            }

            if (mVehicle.getColor() != null) {
                mBinding.view.setBackgroundColor(Color.parseColor(mVehicle.getColor()));
            }

            mBinding.setVehicle(mVehicle);

        }
    }

    private void chooseColor(View v) {
        ColorPickerDialog.newBuilder().setDialogTitle(R.string.vehicle_color_picker)
                .setDialogType(ColorPickerDialog.TYPE_CUSTOM).show((Activity)mContext);
    }

    @Override
    public void onColorSelected(int dialogId, int color) {
        mBinding.view.setBackgroundColor(color);
        mVehicle.setColor(String.format("#%06X", (0xFFFFFF & color)));
    }

    @Override
    public void onDialogDismissed(int dialogId) { }

    private void choosePhoto(View v) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_image)),
                Integer.valueOf(v.getTag().toString()));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            Uri selectedImage = data.getData();
            switch (requestCode) {
                case CAR_PHOTO:
                    Picasso.get().load(selectedImage).into(mBinding.ivVehiclePhoto);
                    mBinding.ivVehiclePhoto.setVisibility(View.VISIBLE);
                    mBinding.btnVehiclePhoto.setVisibility(View.GONE);
                    mVehicle.setPhoto(selectedImage.toString());
                    break;
                case LICENSE_PHOTO:
                    Picasso.get().load(selectedImage).into(mBinding.ivPermitPhoto);
                    mBinding.ivPermitPhoto.setVisibility(View.VISIBLE);
                    mBinding.btnPermitPhoto.setVisibility(View.GONE);
                    mVehicle.setRegisterPhoto(selectedImage.toString());
                    break;
                case PERMIT_PHOTO:
                    Picasso.get().load(selectedImage).into(mBinding.ivPermitPhoto);
                    mBinding.ivPermitPhoto.setVisibility(View.VISIBLE);
                    mBinding.btnPermitPhoto.setVisibility(View.GONE);
                    mVehicle.setPermitPhoto(selectedImage.toString());
                    break;
            }
        }
    }


}
