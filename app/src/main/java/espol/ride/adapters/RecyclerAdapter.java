package espol.ride.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import espol.ride.R;
import espol.ride.databinding.ItemPassengerBinding;
import espol.ride.databinding.ItemRouteBinding;
import espol.ride.databinding.ItemVehicleBinding;
import espol.ride.models.Route;
import espol.ride.models.Vehicle;
import espol.ride.models.driverhome.Passenger;


public class RecyclerAdapter<T> extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    public interface OnItemClickListener<T> {

        boolean onItemClick(T item);

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ViewDataBinding binding;

        ViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private OnItemClickListener<T> mItemListener;

    private ArrayList<T> mDataSet;
    private int mLayoutId;

    public RecyclerAdapter(ArrayList<T> dataSet, int layoutId,
                           OnItemClickListener<T> itemListener) {
        mDataSet = dataSet;
        mLayoutId = layoutId;
        mItemListener = itemListener;
    }

    @NonNull
    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        ViewDataBinding binding =
                DataBindingUtil.inflate(inflater, mLayoutId, viewGroup, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerAdapter.ViewHolder viewHolder, final int position) {

        final ViewDataBinding binding = viewHolder.binding;

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemListener.onItemClick(mDataSet.get(viewHolder.getAdapterPosition()));
            }
        });

        if (binding instanceof ItemPassengerBinding) {

             Passenger passenger = (Passenger) mDataSet.get(position);
            ((ItemPassengerBinding) binding).setPassenger(passenger);
            Picasso.get().load(passenger.getIdProfile().getPhoto()).into(((ItemPassengerBinding) binding).passengerPic);

        } else if (binding instanceof ItemVehicleBinding) {

            Vehicle vehicle = (Vehicle) mDataSet.get(position);
            ((ItemVehicleBinding) binding).setVehicle(vehicle);
            Picasso.get().load(vehicle.getPhoto()).into(((ItemVehicleBinding) binding).vehiclePic);
            ImageView ivState = ((ItemVehicleBinding) binding).ivState;
            switch (vehicle.getState()) {
                case "PENDIENTE":
//                    Picasso.get().load(R.drawable.ic_pending).into(ivState);
                    ivState.setImageResource(R.drawable.ic_pending);
                    break;
                case "RECHAZADO":
//                    Picasso.get().load(R.drawable.ic_rejected).into(ivState);
                    ivState.setImageResource(R.drawable.ic_rejected);
                    break;
                case "ACEPTADO":
//                    Picasso.get().load(R.drawable.ic_check).into(ivState);
                    ivState.setImageResource(R.drawable.ic_check);
                    break;
            }

        } else if (binding instanceof ItemRouteBinding) {

            Route route = (Route) mDataSet.get(position);
            ((ItemRouteBinding) binding).setRoute(route);

        }
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

}

