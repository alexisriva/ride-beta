package espol.ride.application;

import espol.ride.R;
import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;


public class Application extends android.app.Application {

    public static String ADMIN_CHANNEL_ID = "RIDE NOTIFICATION CHANNEL";

    @Override
    public void onCreate() {
        super.onCreate();

        // Calligraphy
        ViewPump.init(ViewPump.builder()
            .addInterceptor(new CalligraphyInterceptor(
                    new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/Questrial/Questrial-Regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
            )).build()
        );
    }
}
