package espol.ride.fragments;


import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import espol.ride.databinding.FragmentPostTripBinding;
import espol.ride.models.Point;
import espol.ride.models.RouteModel;
import espol.ride.models.Vehicle;
import espol.ride.models.riderhome.AvalibleTrip;
import espol.ride.models.riderhome.Route;
import espol.ride.utils.APIRide;
import espol.ride.utils.MapsUtils;
import espol.ride.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostTripFragment extends Fragment implements OnMapReadyCallback {

    private final String TAG = PostTripFragment.class.getSimpleName();
    private Context mContext;

    // Required to see the map
    private GoogleMap mMap;
    private CameraPosition mCameraPosition;

    // The entry point to the Fused Location Provider.
    private FusedLocationProviderClient mFusedLocationProviderClient;

    // A default location (Guayaquil, Ecuador) and default zoom to use when location permission is
    // not granted.
    private final LatLng mDefaultLocation = new LatLng(-2.1693498, -79.9227274);
    private static final int DEFAULT_ZOOM = 15;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean mLocationPermissionGranted;

    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private Location mLastKnownLocation;

    // Keys for storing activity state.
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";

    // List of markers visible on the map
    private List<Marker> mMarkers = new ArrayList<>();
    private Map<Marker, AvalibleTrip> mMarkerTripMap = new Hashtable<>();

    private ArrayList<Vehicle> mVehicles;
    private ArrayList<RouteModel> mRoutes;

    FragmentPostTripBinding mBinding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = getContext();

        // Inflate the layout for this fragment
        mBinding = FragmentPostTripBinding.inflate(inflater, container, false);

        if (savedInstanceState != null) {
            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
        }

        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices
                .getFusedLocationProviderClient(mContext);

        mBinding.map.onCreate(savedInstanceState);
        mBinding.map.getMapAsync(this);

        getVehicles();
        getRoutes();

        mBinding.tripDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showDatePickerDialog(getActivity().getFragmentManager(),
                        new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        final String selectedDate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                        mBinding.tripDate.setText(selectedDate);
                    }
                });
            }
        });

        mBinding.startTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showTimePickerDialog(getActivity().getFragmentManager(),
                        new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                        final String selectedTime = hourOfDay + ":" + minute;
                        mBinding.startTime.setText(selectedTime);
                    }
                });
            }
        });

        mBinding.endTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showTimePickerDialog(getActivity().getFragmentManager(),
                        new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                        final String selectedTime = hourOfDay + ":" + minute;
                        mBinding.endTime.setText(selectedTime);
                    }
                });
            }
        });

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        mBinding.map.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mBinding.map.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            mBinding.map.onDestroy();
        } catch (Exception e) { e.printStackTrace(); }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mBinding.map.onLowMemory();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        if (mMap != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, mMap.getCameraPosition());
            outState.putParcelable(KEY_LOCATION, mLastKnownLocation);
            super.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.getUiSettings().setRotateGesturesEnabled(false);

        // Prompt the user for permission.
        getLocationPermission();

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI();

        // Get the current location of the device and set the position of the map.
        getDeviceLocation();
    }

    /**
     * Gets the current location of the device, and positions the map's camera.
     */
    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionGranted) {
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(getActivity(), new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = task.getResult();
                            if (mLastKnownLocation != null) {
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                        new LatLng(mLastKnownLocation.getLatitude(),
                                                mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                            } else {
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation,
                                        DEFAULT_ZOOM));
                            }
                        } else {
                            Log.d(TAG, "Current location is null. Using defaults.");
                            Log.e(TAG, "Exception: %s", task.getException());
                            mMap.moveCamera(CameraUpdateFactory
                                    .newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                            mMap.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                });
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    /**
     * Prompts the user for permission to use the device location.
     */
    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();
    }

    /**
     * Updates the map's UI settings based on whether the user has granted location permission.
     */
    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private void setUpVehiclesAutoText(final AutoCompleteTextView autoText, ArrayList<Vehicle> list) {
        ArrayAdapter<Vehicle> adapter =
                new ArrayAdapter<>(mContext, android.R.layout.select_dialog_item, list);
        autoText.setThreshold(0);
        autoText.setAdapter(adapter);
        autoText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoText.showDropDown();
            }
        });
    }

    private void setUpRoutesAutoText(final AutoCompleteTextView autoText, ArrayList<RouteModel> list) {
        ArrayAdapter<RouteModel> adapter =
                new ArrayAdapter<>(mContext, android.R.layout.select_dialog_item, list);
        autoText.setThreshold(0);
        autoText.setAdapter(adapter);
        autoText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoText.showDropDown();
            }
        });

        autoText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (Marker m : mMarkers)
                    m.remove();
                mMarkers.clear();

                RouteModel selectedRoute = (RouteModel) parent.getSelectedItem();
                double sumLat = 0;
                double sumLng = 0;
                for (Point p : selectedRoute.getStops()) {
                    MarkerOptions m = new MarkerOptions()
                            .position(new LatLng(p.getLatitude(), p.getLongitude()))
                            .title(p.getDescription());
                    Marker marker = mMap.addMarker(m);
                    mMarkers.add(marker);
                    sumLat += p.getLatitude();
                    sumLng += p.getLongitude();
                }

                double avgLat = sumLat/mMarkers.size();
                double avgLng = sumLng/mMarkers.size();
                MapsUtils.centerMap(mMap, avgLat, avgLng);
            }
        });
    }

    private void getVehicles() {
        APIRide.get(APIRide.APPROVED_VEHICLES, null, new Utils.CustomJsonHttpResponseHandler(mContext) {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                Type type = new TypeToken<ArrayList<Vehicle>>(){}.getType();
                mVehicles = new Gson().fromJson(response.toString(), type);
                setUpVehiclesAutoText(mBinding.tripVehicle, mVehicles);
            }
        });
    }

    private void getRoutes() {
        APIRide.get(APIRide.ROUTES, null, new Utils.CustomJsonHttpResponseHandler(mContext) {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                Type type = new TypeToken<ArrayList<RouteModel>>(){}.getType();
                mRoutes = new Gson().fromJson(response.toString(), type);
                setUpRoutesAutoText(mBinding.tripRoute, mRoutes);
            }
        });
    }
}
