package espol.ride.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import espol.ride.R;
import espol.ride.activities.ListActivity;
import espol.ride.databinding.FragmentProfileBinding;
import espol.ride.models.Student;
import espol.ride.utils.SessionManager;
import espol.ride.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final Context context = getContext();

        // Inflate the layout for this fragment
        FragmentProfileBinding binding =
                FragmentProfileBinding.inflate(inflater, container, false);

        final SessionManager sm = new SessionManager(context);

        Student student = new Gson().fromJson(sm.getStudent(), Student.class);

        binding.setStudent(student);

        Picasso.get().load(student.getPhoto()).into(binding.profilePhoto);

        binding.logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sm.logout();
            }
        });

        if (sm.getRole().equals(Utils.RIDER)) {
            binding.vehicleList.setVisibility(View.GONE);
            binding.routeList.setVisibility(View.GONE);
            binding.swapRole.setText(context.getString(R.string.swap_role, Utils.DRIVER));
        } else {
            binding.swapRole.setText(context.getString(R.string.swap_role, Utils.RIDER));
        }

        binding.vehicleList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ListActivity.class);
                intent.putExtra(Utils.GO_TO, Utils.VEHICLES);
                startActivity(intent);
            }
        });

        binding.routeList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ListActivity.class);
                intent.putExtra(Utils.GO_TO, Utils.ROUTES);
                startActivity(intent);
            }
        });

        binding.swapRole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swapRole(sm);
            }
        });

        return binding.getRoot();
    }

    private void swapRole(SessionManager sessionManager) {
        if (sessionManager.getRole().equals(Utils.RIDER)) {
            sessionManager.setRole(Utils.DRIVER);
            Utils.shortToast(getContext(), "Your role changed\n Now you're a driver");
        } else {
            sessionManager.setRole(Utils.RIDER);
            Utils.shortToast(getContext(), "Your role changed\n Now you're a rider");
        }
        Intent i = getActivity().getIntent();
        getActivity().finish();
        startActivity(i);
    }

}
