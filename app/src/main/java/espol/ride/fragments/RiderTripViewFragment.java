package espol.ride.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import espol.ride.R;
import espol.ride.activities.MainBaseActivity;
import espol.ride.databinding.FragmentRiderTripViewBinding;
import espol.ride.models.riderhomeregistered.RiderHomeRegistered;
import espol.ride.utils.APIRide;
import espol.ride.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class RiderTripViewFragment extends Fragment {

    private FragmentRiderTripViewBinding mBinding;
    private RiderHomeRegistered mRegistered;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = FragmentRiderTripViewBinding.inflate(inflater, container, false);

        getTripInfo();

        mBinding.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelTrip();
            }
        });

        return mBinding.getRoot();
    }

    private void getTripInfo() {
        APIRide.get(APIRide.REGISTERED, null,
                new Utils.CustomJsonHttpResponseHandler(getContext()) {
            @Override
            public void onStart() {
                Utils.showProgressDialog(getContext(), "Getting trip info");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Utils.hideProgressDialog();

                Log.i("RESPONSE REGISTERED", response.toString());
                mRegistered = new Gson().fromJson(response.toString(), RiderHomeRegistered.class);
                mBinding.setRegistered(mRegistered);
                mBinding.driverName.setText(
                        Utils.toTitle(mRegistered.getTrip().getDriver().getId_profile().getName()));
                Picasso.get().load(mRegistered.getTrip().getDriver().getId_profile().getPhoto())
                        .into(mBinding.driverPic);
                Picasso.get().load(mRegistered.getTrip().getVehicle().getPhoto())
                        .into(mBinding.vehiclePic);
                mBinding.vehicleColor.setBackgroundColor(
                        Color.parseColor(mRegistered.getTrip().getVehicle().getColor()));
                buttonDecider(mRegistered.getTrip().getState());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Utils.hideProgressDialog();
                Utils.shortToast(getContext(), "We've encountered problems, please try again later");
            }
        });
    }

    private void cancelTrip() {
        final Context context = getContext();
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                APIRide.delete(mRegistered.getUrl(), null,
                        new Utils.CustomJsonHttpResponseHandler(context) {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Utils.shortToast(context, "Ha sido eliminado del viaje");
                        Activity activity = getActivity();
                        if (activity instanceof MainBaseActivity)
                            ((MainBaseActivity) activity).setPagerAdapter(true, false);
                    }
                });
            }
        };

        Utils.showDialogYesNo(context, R.drawable.ic_warning, "Advertencia",
                "¿Seguro desea eliminar este viaje?", listener, null);
    }

    private void buttonDecider(String status) {
        if (status.equals("PUBLICADO")) {
            mBinding.btnRate.setVisibility(View.GONE);
        } else if (status.equals("EN PROGRESO")) {
            mBinding.btnRate.setVisibility(View.GONE);
            mBinding.btnCancel.setVisibility(View.GONE);
        } else {
            mBinding.btnCancel.setVisibility(View.GONE);
            mBinding.btnRate.setEnabled(true);
            mBinding.btnRate.setVisibility(View.VISIBLE);
        }
    }
}
