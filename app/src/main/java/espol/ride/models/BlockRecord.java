package espol.ride.models;


import java.util.Date;

public class BlockRecord {

    private int id;
    private int blocker;
    private int blocked;
    private Date date;

    public BlockRecord(){}

    public BlockRecord(int id, int blocker, int blocked, Date date) {
        this.id = id;
        this.blocker = blocker;
        this.blocked = blocked;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBlocker() {
        return blocker;
    }

    public void setBlocker(int blocker) {
        this.blocker = blocker;
    }

    public int getBlocked() {
        return blocked;
    }

    public void setBlocked(int blocked) {
        this.blocked = blocked;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "BlockRecord{" +
                "id=" + id +
                ", blocker=" + blocker +
                ", blocked=" + blocked +
                ", date=" + date +
                '}';
    }

}
