package espol.ride.models;


import espol.ride.utils.APIRide;

public class DriverProfile {

    private int id;
    private double rating;
    private double ratingNumber;
    private String state;
    private String licensePhoto;
    private String idProfile;
    private int idRole;


    public DriverProfile() { }

    public DriverProfile(int id, double rating, double ratingNumber, String state,
                         String licensePhoto, String idProfile, int idRole) {
        this.id = id;
        this.rating = rating;
        this.ratingNumber = ratingNumber;
        this.state = state;
        this.licensePhoto = licensePhoto;
        this.idProfile = idProfile;
        this.idRole = idRole;
    }

    public int getId() {
        return id;
    }


    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public double getRatingNumber() {
        return ratingNumber;
    }

    public void setRatingNumber(double ratingNumber) {
        this.ratingNumber = ratingNumber;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLicensePhoto() {
        return licensePhoto;
    }

    public void setLicensePhoto(String licensePhoto) {
        this.licensePhoto = licensePhoto;
    }

    public String getIdProfile() {
        return idProfile;
    }

    public void setIdProfile(String idProfile) {
        this.idProfile = idProfile;
    }

    public int getIdRole() {
        return idRole;
    }

    public void setIdRole(int idRole) {
        this.idRole = idRole;
    }

    public String getUrl() {
        return APIRide.DRIVERPROFILES + id + "/";
    }

    @Override
    public String toString() {
        return "DriverProfile{" +
                "id=" + id +
                ", rating=" + rating +
                ", ratingNumber=" + ratingNumber +
                ", state='" + state + '\'' +
                ", licensePhoto='" + licensePhoto + '\'' +
                ", idProfile='" + idProfile + '\'' +
                ", idRole=" + idRole +
                '}';
    }

}