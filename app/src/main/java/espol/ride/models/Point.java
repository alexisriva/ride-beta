package espol.ride.models;


import com.google.android.gms.maps.model.Marker;

import espol.ride.utils.APIRide;


public class Point {

    private int id;
    private double latitude;
    private double longitude;
    private String description;

    public Point() {}

    public Point(Marker marker) {
        this.latitude = marker.getPosition().latitude;
        this.longitude = marker.getPosition().longitude;
        this.description = marker.getTitle();
    }

    public Point(int id, double latitude, double longitude, String description) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.description = description;
    }

    public Point(double latitude, double longitude, String description) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getUrl() {
        return String.format("%s%d/", APIRide.POINTS, id);
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }

    public boolean equals(Object o) {
        if (this == o)
            return true;

        Point p;
        if (o instanceof Marker) {
            Marker m = (Marker) o;
            p = new Point();
            p.latitude = m.getPosition().latitude;
            p.longitude = m.getPosition().longitude;
            p.description = m.getTitle();
            return (latitude == p.latitude) && (longitude == p.longitude);
        } else if (o instanceof  Point) {
            p = (Point) o;
            return (id == p.id) && (latitude == p.latitude) && (longitude == p.longitude);
        } else
            return false;
    }

}
