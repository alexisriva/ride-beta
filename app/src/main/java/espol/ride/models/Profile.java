package espol.ride.models;


import java.util.List;

public class Profile {

    private String student_id;
    private String name;
    private String faculty;
    private String photo;
    private int strikes_num;
    private int user;
    private List<Integer> roles;
    private List<Integer> blocked_users;

    public Profile() {}

    public Profile(String student_id, int user, String name, String faculty, String photo,
                   List<Integer> roles, List<Integer> blocked_users, int strikes_num) {
        this.student_id = student_id;
        this.user = user;
        this.name = name;
        this.faculty = faculty;
        this.photo = photo;
        this.roles = roles;
        this.blocked_users = blocked_users;
        this.strikes_num = strikes_num;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public List<Integer> getRoles() {
        return roles;
    }

    public void setRoles(List<Integer> roles) {
        this.roles = roles;
    }

    public List<Integer> getBlocked_users() {
        return blocked_users;
    }

    public void setBlocked_users(List<Integer> blocked_users) {
        this.blocked_users = blocked_users;
    }

    public int getStrikes_num() {
        return strikes_num;
    }

    public void setStrikes_num(int strikes_num) {
        this.strikes_num = strikes_num;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "student_id='" + student_id + '\'' +
                ", user=" + user +
                ", name='" + name + '\'' +
                ", faculty='" + faculty + '\'' +
                ", photo='" + photo + '\'' +
                ", roles=" + roles +
                ", blocked_users=" + blocked_users +
                ", strikes_num=" + strikes_num +
                '}';
    }
}
