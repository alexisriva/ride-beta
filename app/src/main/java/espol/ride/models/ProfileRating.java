package espol.ride.models;


public class ProfileRating {

    private int id;
    private int route;
    private int rater;
    private int rated;
    private float rating;
    private String description;

    public ProfileRating() {}

    public ProfileRating(int id, int route, int rater, int rated, float rating, String description) {
        this.id = id;
        this.route = route;
        this.rater = rater;
        this.rated = rated;
        this.rating = rating;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRoute() {
        return route;
    }

    public void setRoute(int route) {
        this.route = route;
    }

    public int getRater() {
        return rater;
    }

    public void setRater(int rater) {
        this.rater = rater;
    }

    public int getRated() {
        return rated;
    }

    public void setRated(int rated) {
        this.rated = rated;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "ProfileRating{" +
                "id=" + id +
                ", route=" + route +
                ", rater=" + rater +
                ", rated=" + rated +
                ", rating=" + rating +
                ", description='" + description + '\'' +
                '}';
    }

}
