package espol.ride.models;


public class ProfileRole {

    private int id;
    private double rating;
    private int rating_number;
    private String id_profile;
    private int id_role;

    public ProfileRole() { }

    public ProfileRole(int id, double rating, int rating_number, String id_profile, int id_role) {
        this.id = id;
        this.rating = rating;
        this.rating_number = rating_number;
        this.id_profile = id_profile;
        this.id_role = id_role;
    }

    public int getId() {
        return id;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getRating_number() {
        return rating_number;
    }

    public void setRating_number(int rating_number) {
        this.rating_number = rating_number;
    }

    public String getId_profile() {
        return id_profile;
    }

    public void setId_profile(String id_profile) {
        this.id_profile = id_profile;
    }

    public int getId_role() {
        return id_role;
    }

    public void setId_role(int id_role) {
        this.id_role = id_role;
    }

    @Override
    public String toString() {
        return "ProfileRole{" +
                "id=" + id +
                ", rating=" + rating +
                ", rating_number=" + rating_number +
                ", id_profile='" + id_profile + '\'' +
                ", id_role=" + id_role +
                '}';
    }
}
