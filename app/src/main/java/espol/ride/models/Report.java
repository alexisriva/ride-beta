package espol.ride.models;


public class Report {

    private int id;
    private int reporter;
    private int reported;
    private String subject;
    private String body;

    public Report(int id, int reporter, int reported, String subject, String body) {
        this.id = id;
        this.reporter = reporter;
        this.reported = reported;
        this.subject = subject;
        this.body = body;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getReporter() {
        return reporter;
    }

    public void setReporter(int reporter) {
        this.reporter = reporter;
    }

    public int getReported() {
        return reported;
    }

    public void setReported(int reported) {
        this.reported = reported;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Report{" +
                "id=" + id +
                ", reporter=" + reporter +
                ", reported=" + reported +
                ", subject='" + subject + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
