package espol.ride.models;


public class Request {

    private int id;
    private int route;
    private int requester;
    private int status;

    public Request(int id, int route, int requester, int status) {
        this.id = id;
        this.route = route;
        this.requester = requester;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRoute() {
        return route;
    }

    public void setRoute(int route) {
        this.route = route;
    }

    public int getRequester() {
        return requester;
    }

    public void setRequester(int requester) {
        this.requester = requester;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Request{" +
                "id=" + id +
                ", route=" + route +
                ", requester=" + requester +
                ", status=" + status +
                '}';
    }

}
