package espol.ride.models;


import android.support.annotation.NonNull;

import java.util.List;

public class Route {
    private int id;
    private String name;
    private boolean frequent;
    private int driver;
    private List<Point> stops;

    public Route() { }

    public Route(String name, boolean frequent, int driver, List<Point> stops) {
        this.name = name;
        this.frequent = frequent;
        this.driver = driver;
        this.stops = stops;
    }

    public Route(int id, String name, boolean frequent, int driver, List<Point> stops) {
        this.id = id;
        this.name = name;
        this.frequent = frequent;
        this.driver = driver;
        this.stops = stops;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isFrequent() {
        return frequent;
    }

    public void setFrequent(boolean frequent) {
        this.frequent = frequent;
    }

    public int getDriver() {
        return driver;
    }

    public void setDriver(int driver) {
        this.driver = driver;
    }

    public List<Point> getStops() {
        return stops;
    }

    public void setStops(List<Point> stops) {
        this.stops = stops;
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }
}