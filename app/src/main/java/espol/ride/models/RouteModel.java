package espol.ride.models;


import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class RouteModel {

    private int id;
    private String name;
    private String start_zone;
    private String end_zone;
    private int driver;
    private List<Point> stops;

    public RouteModel() {
        stops = new ArrayList<>();
    }

    public RouteModel(int id, String name, String start_zone, String end_zone,
                      int driver, List<Point> stops) {
        this.id = id;
        this.name = name;
        this.start_zone = start_zone;
        this.end_zone = end_zone;
        this.driver = driver;
        this.stops = stops;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStart_zone() {
        return start_zone;
    }

    public void setStart_zone(String start_zone) {
        this.start_zone = start_zone;
    }

    public String getEnd_zone() {
        return end_zone;
    }

    public void setEnd_zone(String end_zone) {
        this.end_zone = end_zone;
    }

    public int getDriver() {
        return driver;
    }

    public void setDriver(int driver) {
        this.driver = driver;
    }

    public List<Point> getStops() {
        return stops;
    }

    public void setStops(List<Point> stops) {
        this.stops = stops;
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }

}
