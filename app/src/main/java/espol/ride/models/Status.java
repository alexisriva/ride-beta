package espol.ride.models;


import espol.ride.utils.APIRide;

public class Status {

    private int id;
    private String name;
    private String description;

    public Status() {}

    public Status(int id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Status(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getUrl() {
        return String.format("%s%d/", APIRide.STATUSES, id);
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Status{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
