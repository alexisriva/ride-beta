package espol.ride.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Student {

    private static final String FIEC = "FIEC";
    private static final String FICT = "FICT";
    private static final String FIMCP = "FIMCP";
    private static final String FCNM = "FCNM";
    private static final String FICSH = "FICSH";
    private static final String FIMCM = "FIMCM";
    private static final String FCV = "FCV";
    private static final String FADCOM = "FADCOM";

    private String id, user, userId, firstName, lastName, school, photo;

    public Student(String[] data) {
        user = data[0];
        userId = data[1];
        id = data[2];
        firstName = data[3];
        lastName = data[4];
        school = data[5];
        photo = data[6];
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        String studentName = firstName + " " + lastName;
        StringBuilder resultPlaceHolder = new StringBuilder(studentName.length());
        List<String> fullName = new ArrayList<>(Arrays.asList(studentName.toLowerCase().split(" ")));
        if (fullName.size() == 4) {
            fullName.remove(1);
            fullName.remove(2);
        } else if (fullName.size() == 3) {
            fullName.remove(2);
        }
        for (String name : fullName) {
            char[] charWord = name.toCharArray();
            charWord[0] = Character.toTitleCase(charWord[0]);
            resultPlaceHolder.append(new String(charWord)).append(" ");
        }
        return resultPlaceHolder.toString().trim();
    }

    public String getSchool() {
        if (school.toLowerCase().contains("electricidad"))
            return FIEC;
        else if (school.toLowerCase().contains("tierra"))
            return FICT;
        else if (school.toLowerCase().contains("mecánica"))
            return FIMCP;
        else if (school.toLowerCase().contains("naturales"))
            return FCNM;
        else if (school.toLowerCase().contains("sociales"))
            return FICSH;
        else if (school.toLowerCase().contains("marítima"))
            return FIMCM;
        else if (school.toLowerCase().contains("vida"))
            return FCV;
        else if (school.toLowerCase().contains("audiovisual"))
            return FADCOM;
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id='" + id + '\'' +
                ", user='" + user + '\'' +
                ", userId='" + userId + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", school='" + school + '\'' +
                ", photo='" + photo + '\'' +
                '}';
    }
}
