package espol.ride.models;


import java.sql.Timestamp;
import java.util.List;

import espol.ride.utils.APIRide;

public class Trip {

    private int id;

    private int route;
    private int vehicle;
    private int driver;
    private int status;

    private int seats_number;
    private Timestamp date;
    private Timestamp start_time;
    private Timestamp end_time;

    private Timestamp stimated_start_time;
    private Timestamp stimated_end_time;
    private String trip_type;

    private List<ProfileRole> passengers;

    public Trip() {}

    public Trip(int id, int route, int vehicle, int driver, int status, Timestamp start_time, Timestamp end_time) {
        this.id = id;
        this.route = route;
        this.vehicle = vehicle;
        this.driver = driver;
        this.status = status;
        this.start_time = start_time;
        this.end_time = end_time;
    }

    public String getUrl() {
        return String.format("%s%d/", APIRide.TRIPS, id);
    }

    public int getId() {
        return id;
    }

    public int getRoute() {
        return route;
    }

    public void setRoute(int route) {
        this.route = route;
    }

    public int getVehicle() {
        return vehicle;
    }

    public void setVehicle(int vehicle) {
        this.vehicle = vehicle;
    }

    public int getDriver() {
        return driver;
    }

    public void setDriver(int driver) {
        this.driver = driver;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Timestamp getStart_time() {
        return start_time;
    }

    public void setStart_time(Timestamp start_time) {
        this.start_time = start_time;
    }

    public Timestamp getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Timestamp end_time) {
        this.end_time = end_time;
    }

    public int getSeats_number() {
        return seats_number;
    }

    public void setSeats_number(int seats_number) {
        this.seats_number = seats_number;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "route=" + route +
                ", vehicle=" + vehicle +
                ", driver=" + driver +
                ", status=" + status +
                ", start_time=" + start_time +
                ", end_time=" + end_time +
                '}';
    }
}
