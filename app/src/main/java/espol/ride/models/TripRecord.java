package espol.ride.models;


public class TripRecord {

    private int id;
    private int passenger;
    private int route;

    public TripRecord(int id, int passenger, int route) {
        this.id = id;
        this.passenger = passenger;
        this.route = route;
    }

    public int getId() {
        return id;
    }

    public int getPassenger() {
        return passenger;
    }

    public void setPassenger(int passenger) {
        this.passenger = passenger;
    }

    public int getRoute() {
        return route;
    }

    public void setRoute(int route) {
        this.route = route;
    }

    @Override
    public String toString() {
        return "TripRecord{" +
                "id=" + id +
                ", passenger=" + passenger +
                ", route=" + route +
                '}';
    }

}
