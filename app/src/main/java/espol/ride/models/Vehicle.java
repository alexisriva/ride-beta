package espol.ride.models;


import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Vehicle {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("license_plate")
    @Expose
    private String licensePlate;
    @SerializedName("brand")
    @Expose
    private String brand;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("register_photo")
    @Expose
    private String registerPhoto;
    @SerializedName("permit_photo")
    @Expose
    private String permitPhoto;
    @SerializedName("owner")
    @Expose
    private int owner;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getRegisterPhoto() {
        return registerPhoto;
    }

    public void setRegisterPhoto(String registerPhoto) {
        this.registerPhoto = registerPhoto;
    }

    public String getPermitPhoto() {
        return permitPhoto;
    }

    public void setPermitPhoto(String permitPhoto) {
        this.permitPhoto = permitPhoto;
    }

    public int getOwner() {
        return owner;
    }

    public void setOwner(int owner) {
        this.owner = owner;
    }

    @NonNull
    @Override
    public String toString() {
        return licensePlate;
    }
}