package espol.ride.models;


public class Zone {

    private int id;
    private String url;
    private String name;
    private String description;

    public Zone() {}

    public Zone(String name) {
        this.name = name;
    }

    public Zone(int id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Zone(String url, String name, String description) {
        this.url = url;
        this.name = name;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Zone) {
            Zone o = (Zone) obj;
            return name.equals(o.name);
        }
        return super.equals(obj);
    }
}
