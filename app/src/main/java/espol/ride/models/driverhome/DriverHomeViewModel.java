package espol.ride.models.driverhome;

import java.util.ArrayList;
import java.util.List;

public class DriverHomeViewModel {

    private int id;
    private Route route;
    private int vehicle;
    private String date;
    private String estimated_start_time;
    private String estimated_end_time;
    private String trip_type;
    private List<TriprecordSet> triprecord_set = new ArrayList<TriprecordSet>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public int getVehicle() {
        return vehicle;
    }

    public void setVehicle(int vehicle) {
        this.vehicle = vehicle;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEstimatedStartTime() {
        return estimated_start_time;
    }

    public void setEstimatedStartTime(String estimatedStartTime) {
        this.estimated_start_time = estimatedStartTime;
    }

    public String getEstimatedEndTime() {
        return estimated_end_time;
    }

    public void setEstimatedEndTime(String estimatedEndTime) {
        this.estimated_end_time = estimatedEndTime;
    }

    public String getTripType() {
        return trip_type;
    }

    public void setTripType(String tripType) {
        this.trip_type = tripType;
    }

    public List<TriprecordSet> getTriprecordSet() {
        return triprecord_set;
    }

    public void setTriprecordSet(List<TriprecordSet> triprecordSet) {
        this.triprecord_set = triprecordSet;
    }

}
