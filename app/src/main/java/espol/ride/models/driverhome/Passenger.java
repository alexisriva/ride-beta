package espol.ride.models.driverhome;


public class Passenger {

    private IdProfile id_profile;
    private int rating;
    private int ratingNumber;

    public IdProfile getIdProfile() {
        return id_profile;
    }

    public void setIdProfile(IdProfile idProfile) {
        this.id_profile = idProfile;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getRatingNumber() {
        return ratingNumber;
    }

    public void setRatingNumber(int ratingNumber) {
        this.ratingNumber = ratingNumber;
    }

}
