package espol.ride.models.driverhome;

import java.util.ArrayList;
import java.util.List;

public class Route {

    private List<Stop> stops = new ArrayList<Stop>();

    public List<Stop> getStops() {
        return stops;
    }

    public void setStops(List<Stop> stops) {
        this.stops = stops;
    }

}
