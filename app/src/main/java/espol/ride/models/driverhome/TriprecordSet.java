package espol.ride.models.driverhome;

import java.util.Locale;

import espol.ride.utils.APIRide;

public class TriprecordSet {

    private int id;
    private Passenger passenger;
    private int stop;

    public String getUrl() {
        return String.format(Locale.getDefault(),"%s%d/", APIRide.TRIPRECORD, id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public int getStop() {
        return stop;
    }

    public void setStop(int stop) {
        this.stop = stop;
    }

}
