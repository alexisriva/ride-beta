package espol.ride.models.riderhome;


public class Driver {

    private Integer rating;
    private Integer rating_number;
    private String id_profile;

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Integer getRatingNumber() {
        return rating_number;
    }

    public void setRatingNumber(Integer ratingNumber) {
        this.rating_number = ratingNumber;
    }

    public String getIdProfile() {
        return id_profile;
    }

    public void setIdProfile(String idProfile) {
        this.id_profile = idProfile;
    }

}
