package espol.ride.models.riderhome;


import java.util.ArrayList;
import java.util.List;

public class RiderHomeViewModel {

    List<AvalibleTrip> avalibleTrips;

    public RiderHomeViewModel() {
        this.avalibleTrips = new ArrayList<>();
    }

    public RiderHomeViewModel(List<AvalibleTrip> avalibleTrips) {
        this.avalibleTrips = avalibleTrips;
    }

}
