package espol.ride.models.riderhome;

import java.util.List;

public class Route {

    private List<Stop> stops = null;

    public List<Stop> getStops() {
        return stops;
    }

    public void setStops(List<Stop> stops) {
        this.stops = stops;
    }

}
