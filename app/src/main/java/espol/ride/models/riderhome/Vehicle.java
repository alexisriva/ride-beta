package espol.ride.models.riderhome;


public class Vehicle {

    private String color;
    private String license_plate;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getLicensePlate() {
        return license_plate;
    }

    public void setLicensePlate(String licensePlate) {
        this.license_plate = licensePlate;
    }

}
