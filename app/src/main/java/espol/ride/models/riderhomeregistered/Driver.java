package espol.ride.models.riderhomeregistered;


public class Driver {

    private IdProfile id_profile;
    private double rating;
    private int rating_number;

    public IdProfile getId_profile() {
        return id_profile;
    }

    public void setId_profile(IdProfile id_profile) {
        this.id_profile = id_profile;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getRating_number() {
        return rating_number;
    }

    public void setRating_number(int rating_number) {
        this.rating_number = rating_number;
    }

}
