package espol.ride.models.riderhomeregistered;

import java.util.Locale;

import espol.ride.utils.APIRide;

public class RiderHomeRegistered {

    private int id;
    private Stop stop;
    private Trip trip;
    private int estimatedTime;
    private int estimatedDistance;
    private boolean visited;

    public String getUrl() {
        return String.format(Locale.getDefault(),"%s%d/", APIRide.TRIPRECORD, id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Stop getStop() {
        return stop;
    }

    public void setStop(Stop stop) {
        this.stop = stop;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public int getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(int estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public int getEstimatedDistance() {
        return estimatedDistance;
    }

    public void setEstimatedDistance(int estimatedDistance) {
        this.estimatedDistance = estimatedDistance;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }
}
