package espol.ride.models.riderhomeregistered;


public class Stop {

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
