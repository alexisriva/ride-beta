package espol.ride.models.riderhomeregistered;


public class Trip {

    private Driver driver;
    private Vehicle vehicle;
    private String estimated_start_time;
    private String estimated_end_time;
    private String state;

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public String getEstimated_start_time() {
        return estimated_start_time;
    }

    public void setEstimated_start_time(String estimated_start_time) {
        this.estimated_start_time = estimated_start_time;
    }

    public String getEstimated_end_time() {
        return estimated_end_time;
    }

    public void setEstimated_end_time(String estimated_end_time) {
        this.estimated_end_time = estimated_end_time;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

}
