package espol.ride.models.trip;


public class Driver {

    private int id;
    private int rating;
    private int ratingNumber;
    private String profile;
    private int idRole;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getRatingNumber() {
        return ratingNumber;
    }

    public void setRatingNumber(int ratingNumber) {
        this.ratingNumber = ratingNumber;
    }

    public String getIdProfile() {
        return profile;
    }

    public void setIdProfile(String profile) {
        this.profile = profile;
    }

    public int getIdRole() {
        return idRole;
    }

    public void setIdRole(int idRole) {
        this.idRole = idRole;
    }

}
