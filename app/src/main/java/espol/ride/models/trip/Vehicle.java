package espol.ride.models.trip;

public class Vehicle {
    private int id;
    private String license_plate_letters;
    private String license_plate_numbers;

    private String license_plate;
    private String brand;
    private String model;
    private String color;
    private Driver owner;

}
