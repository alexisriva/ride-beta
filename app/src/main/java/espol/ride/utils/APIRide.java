package espol.ride.utils;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.SyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;

public class APIRide {

    private static final String URL = "http://testride.espol.edu.ec/api/";
//    private static final String URL = "http://192.168.100.149:8000/api/";

    public static final String CHECK_USER = URL + "check-user/";
    public static final String NEW_USER = URL + "new-user/";

    public static final String TOKEN_AUTH = URL + "token-auth/";
    public static final String TOKEN_VERIFY = URL + "token-verify/";

    public static final String PROFILES = URL + "profiles/";
    public static final String DRIVERPROFILES = URL + "driver-profiles/";

    public static final String VEHICLES = URL + "vehicles/";
    public static final String APPROVED_VEHICLES = URL + "approved-vehicles/";

    public static final String ROUTES = URL + "routes/";

    public static final String STATUSES = URL + "statuses/";

    public static final String TRIPS = URL + "trips/";
    public static final String AVAILABLETRIPS = TRIPS + "available/";
    public static final String CURRENTTRIP = TRIPS + "active/";
    public static final String HASACTIVE = TRIPS + "has_active/";

    public static final String TRIPRECORD = URL + "triprecords/";
    public static final String REGISTERED = TRIPRECORD + "registered/";
    public static final String ISREGISTERED = TRIPRECORD + "is_registered/";

    public static final String POINTS = URL + "points/";
    public static final String TRIPREGISTER = URL + "trip-register/";

    public static final String DEVICES = URL + "devices/";

    public static final String CHANGEPASSWORD = URL + "auth-change-password/";

    public static final String BRANDS = URL + "brands/";


    private static AsyncHttpClient asyncClient = new AsyncHttpClient();
    private static SyncHttpClient syncClient = new SyncHttpClient();


    public static void addAuth(String token) {
        asyncClient.addHeader("Authorization", "JWT " + token);
    }

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        asyncClient.get(url, params, responseHandler);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        asyncClient.post(url, params, responseHandler);
    }

    public static void post(Context context, String url, StringEntity entity, AsyncHttpResponseHandler responseHandler) {
        asyncClient.post(context, url, entity, ContentType.APPLICATION_JSON.getMimeType(), responseHandler);
    }

    public static void syncPost(String url, RequestParams params, JsonHttpResponseHandler responseHandler) {
        syncClient.post(url, params, responseHandler);
    }

    public static void put(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        asyncClient.put(url, params, responseHandler);
    }

    public static void put(Context context, String url, StringEntity entity, AsyncHttpResponseHandler responseHandler) {
        asyncClient.put(context, url, entity, ContentType.APPLICATION_JSON.getMimeType(), responseHandler);
    }

    public static void patch(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        asyncClient.patch(url, params, responseHandler);
    }

    public static void delete(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        asyncClient.delete(url, params, responseHandler);
    }

}
