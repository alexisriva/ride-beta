package espol.ride.utils;


import android.app.Activity;
import android.location.Location;
import android.support.annotation.NonNull;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;


public class MapsUtils {

    public static final int DEFAULT_ZOOM = 11;
    public static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    public static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";
    public static final String KEW_CAMERA_POSITION = "camera_position";
    public static final String KEY_LOCATION = "location";
    public static final LatLng DEFAULT_LOCATION = new LatLng(-2.1693498, -79.9227274);


    public static void centerMap(GoogleMap map, double lat, double lng) {
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(lat, lng), DEFAULT_ZOOM));

    }

    public static void getDeviceLocation(Activity activity, final GoogleMap map,
                                   boolean locationPermissionGranted,
                                   FusedLocationProviderClient fusedLocationProviderClient) {
        try {
            map.moveCamera(CameraUpdateFactory
                    .newLatLngZoom(DEFAULT_LOCATION, DEFAULT_ZOOM));
            map.getUiSettings().setMyLocationButtonEnabled(false);
            if (locationPermissionGranted) {
                Task<Location> locationResult = fusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(activity, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            final Location lastKnownLocation = task.getResult();
                            if (lastKnownLocation != null) {
                                centerMap(map, lastKnownLocation.getLatitude(),
                                        lastKnownLocation.getLongitude());
                            }
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }
}
