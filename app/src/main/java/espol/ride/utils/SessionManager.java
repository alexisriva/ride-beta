package espol.ride.utils;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import espol.ride.activities.LoginActivity;


public class SessionManager {

    private  Context context;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private static String STUDENT = "STUDENT";
    private static String TOKEN = "TOKEN";
    private static String ROLE = "ROLE";
    private static String LOGEDIN = "LOGEDIN";
    private static String REGISTERED = "REGISTERED";
    private static String PUBLISHED = "PUBLISHED";


    public SessionManager(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences("RIDE_ESPOL", Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public void setStudent(String student){
        editor.putString(STUDENT, student);
        editor.apply();
    }

    public String getStudent() {
        return preferences.getString(STUDENT,null);
    }

    public void setToken(String token){
        editor.putString(TOKEN, token);
        editor.apply();
    }

    public String getToken(){
        return preferences.getString(TOKEN,null);
    }

    public void setRole(String role){
        editor.putString(ROLE, role);
        editor.apply();
    }

    public String getRole(){
        return preferences.getString(ROLE,null);
    }

    public void setLogedIn(boolean logedIn) {
        editor.putBoolean(LOGEDIN, logedIn);
        editor.apply();
    }

    public boolean getLogedIn() { return preferences.getBoolean(LOGEDIN, false); }

    public void setRegistered(boolean registered) {
        editor.putBoolean(REGISTERED, registered);
        editor.apply();
    }

    public boolean getRegistered() { return preferences.getBoolean(REGISTERED, false); }

    public void setPublished(boolean published) {
        editor.putBoolean(PUBLISHED, published);
        editor.apply();
    }

    public boolean getPublished() { return preferences.getBoolean(PUBLISHED, false); }

    public void logout() {
        this.setToken(null);
        this.setStudent(null);
        this.setLogedIn(false);
        Intent intent = new Intent(context, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }


//    public void clear(){
//        editor.clear();
//        editor.apply();
//    }
}
