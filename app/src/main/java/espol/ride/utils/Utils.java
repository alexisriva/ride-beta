package espol.ride.utils;


import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import espol.ride.R;

import static android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS;


public class Utils {

    /**
     *  CONSTANTS FOR ACTIVITY FLOW
     */
    public static final String GO_TO = "GO TO";
    public static final String PROFILE = "Profile";
    public static final String VEHICLES = "Vehicles";
    public static final String TRIPS = "Trips";
    public static final String ROUTES = "Routes";
    public static final String RIDER = "rider";
    public static final String DRIVER = "driver";

    /**
     * VARIABLES FOR UI FEEDBACK
     */
    private static ProgressDialog mProgressDialog = null;
    private static Toast mToast = null;
    private static Snackbar mSnackbar = null;

    public static void showProgressDialog(Context context, String messageDetail) {
        if (mProgressDialog == null)
            mProgressDialog = new ProgressDialog(context);

        mProgressDialog.setMessage(messageDetail);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public static void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
        mProgressDialog = null;
    }

    public static void showDialog(Context context, int icon, String title, String message) {
        hideProgressDialog();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title)
                .setMessage(message)
                .setIcon(icon)
                .show();
    }

    public static void showDialogYesNo(Context context, int icon, String title,
                                       String message,
                                       DialogInterface.OnClickListener yesListener,
                                       DialogInterface.OnClickListener noListener) {
        hideProgressDialog();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title)
                .setMessage(message)
                .setIcon(icon)
                .setPositiveButton(R.string.yes, yesListener)
                .setNegativeButton(R.string.no, noListener)
                .show();
    }

    public static void showErrorDialog(Context context, String errorDetail) {
        hideProgressDialog();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.error_prompt)
                .setMessage(errorDetail)
                .setIcon(R.drawable.ic_clear)
                .show();
    }

    public static void showSuccessDialog(Context context, String successDetail) {
        hideProgressDialog();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.success_prompt)
                .setMessage(successDetail)
                .setIcon(R.drawable.ic_check)
                .show();
    }

    public static void showOkSuccessDialog(Context context, String successDetail,
                                           DialogInterface.OnClickListener clickListener) {
        hideProgressDialog();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.success_prompt)
                .setMessage(successDetail)
                .setIcon(R.drawable.ic_check)
                .setPositiveButton(R.string.ok, clickListener)
                .setCancelable(false)
                .show();
    }

    public static void showWarningDialog(Context context, DialogInterface.OnClickListener clickListener) {
        hideProgressDialog();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.warning_prompt)
                .setMessage(R.string.confirmation_prompt)
                .setIcon(R.drawable.ic_warning)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, clickListener)
                .setNegativeButton(R.string.no, clickListener)
                .show();
    }

    public static class CustomJsonHttpResponseHandler extends JsonHttpResponseHandler {
        Context context;

        public CustomJsonHttpResponseHandler(Context context) {
            this.context = context;
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString,
                              Throwable throwable) {
            showErrorDialog(context, responseString);
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                              JSONObject errorResponse) {
            try {
                if (errorResponse != null) {
                    if (errorResponse.get("detail").equals("Signature has expired.")) {
                        showOkSuccessDialog(context, "Sesion caducada",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        SessionManager manager = new SessionManager(context);
                                        manager.logout();
                                    }
                                });
                    } else
                        showErrorDialog(context, errorResponse.toString());
                } else
                    showErrorDialog(context, "Algo raro pasa");
            } catch (Exception e) {
                showErrorDialog(context, e.getMessage());
            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                              JSONArray errorResponse) {
            showErrorDialog(context, errorResponse.toString());
        }

    }

    public static String toTitle(String inputString) {
        StringBuilder resultPlaceHolder = new StringBuilder(inputString.length());
        List<String> fullName = new ArrayList<>(Arrays.asList(inputString.toLowerCase().split(" ")));
        if (fullName.size() == 4) {
            fullName.remove(1);
            fullName.remove(2);
        } else if (fullName.size() == 3) {
            fullName.remove(2);
        }
        for (String name : fullName) {
            char[] charWord = name.toCharArray();
            charWord[0] = Character.toTitleCase(charWord[0]);
            resultPlaceHolder.append(new String(charWord)).append(" ");
        }
        return resultPlaceHolder.toString().trim();
    }

    /**
     * HANDLER FOR TOAST
     **/
    public static void longToast(Context context, String message) {
        if (mToast != null) {
            mToast.cancel();
        }

        mToast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        mToast.show();
    }

    public static void shortToast(Context context, String message) {
        if (mToast != null) {
            mToast.cancel();
        }

        mToast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        mToast.show();
    }

    /**
     * HANDLER FOR GPS
     */
    public static boolean checkGpsEnabled(final Context context, View snackbarParent) {
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (mSnackbar != null) {
                mSnackbar.dismiss();
                mSnackbar = null;
            }

            mSnackbar = Snackbar
                    .make(snackbarParent, "GPS está desactivado",
                            Snackbar.LENGTH_INDEFINITE)
                    .setAction("Activar", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            context.startActivity(new Intent(ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    });

            // Changing message text color
            mSnackbar.setActionTextColor(context.getResources().getColor(R.color.colorAccent));

            // Show snackbar
            mSnackbar.show();
            return false;
        }
        return true;
    }

    /**
     *  HANDLER FOR DATE/TIME PICKER
     */
    public static void showDatePickerDialog(FragmentManager fm, DatePickerDialog.OnDateSetListener listener) {
        final Calendar now = Calendar.getInstance();
        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                listener,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        datePickerDialog.setMinDate(now);

        datePickerDialog.show(fm, "DatePickerDialog");
    }

    public static void showTimePickerDialog(FragmentManager fm, TimePickerDialog.OnTimeSetListener listener) {
        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(listener, true);
        timePickerDialog.setTimeInterval(1, 15);

        timePickerDialog.show(fm, "TimePickerDialog");
    }

    public static String changeDateFormat(String date) {
        String[] dateSplit = date.split("/");

        if (dateSplit.length == 3) {
            int day = Integer.parseInt(dateSplit[0]);
            int month = Integer.parseInt(dateSplit[1]);
            int year = Integer.parseInt(dateSplit[2]);
            return year + "-" + month + "-" + day;
        }

        return date;
    }

    /**
     *  HANDLER FOR TOOLBAR
     */
    public static void setToolbar(Context context, Toolbar toolbar) {
        ((AppCompatActivity) context).setSupportActionBar(toolbar);
        ((AppCompatActivity) context).getSupportActionBar().setTitle("");

        final Drawable upArrow = getUpArrow(context);

        ((AppCompatActivity) context).getSupportActionBar().setHomeAsUpIndicator(upArrow);
        ((AppCompatActivity) context).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public static Drawable getUpArrow(Context context) {
        @SuppressLint("PrivateResource")
        Drawable upArrow = context.getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(context.getResources().getColor(R.color.colorAccent),
                PorterDuff.Mode.SRC_ATOP);
        return upArrow;
    }
}
