package espol.ride.utils;

import android.content.Context;
import android.text.TextUtils;
import android.widget.EditText;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import espol.ride.R;

public class Validator {

    public static int TEXT_FIELD = 0;
    public static int EMAIL_FIELD = 1;
    public static int PASSWORD_FIELD = 2;
    public static int IDENTIFIER_FIELD = 3;
    public static int RUC_FIELD = 4;
    public static int LICENSE_FIELD = 5;
    public static int PHONE_FIELD = 6;


    private static final String EMAIL_PATTERN =
            "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";


    public static boolean isValidString(String text) {
        return text != null && !text.isEmpty();
    }

    public static boolean isValidYear(int year) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return year > 1975 && year <= currentYear;
    }

    public static boolean isValidNumberLength(String number, int lenght) {
        return number.length() == lenght;
    }

    public static boolean isInvalidInput(
            Context context, EditText editText, int field, int messageId) {
        String text = resetErrorAndGetString(editText);

        boolean cancelAttempt = false;

        if (TextUtils.isEmpty(text)) {
            editText.setError(context.getString(R.string.required_field));
            cancelAttempt = true;
        } else {
            if (field == EMAIL_FIELD) {
                if (!isEmailValid(text)) {
                    editText.setError(context.getString(messageId));
                    cancelAttempt = true;
                }
            } else if (field == PASSWORD_FIELD) {
                if (!isValidPassword(text)) {
                    editText.setError(context.getString(messageId));
                    cancelAttempt = true;
                }
            } else if (field == IDENTIFIER_FIELD || field == LICENSE_FIELD ||
                    field == PHONE_FIELD) {
                if (!isValidIdLength(text)) {
                    editText.setError(context.getString(messageId));
                    cancelAttempt = true;
                }
            } else if (field == RUC_FIELD) {
                if (!isValidRucLength(text)) {
                    editText.setError(context.getString(messageId));
                    cancelAttempt = true;
                }
            }
        }

        return cancelAttempt;
    }

    private static String resetErrorAndGetString(EditText editText) {
        // Reset void
        editText.setError(null);

        return editText.getText().toString().trim();
    }

    private static boolean isEmailValid(String email) {
        try {
            // Compiles the given regular expression into a pattern.
            Pattern pattern = Pattern.compile(EMAIL_PATTERN);
            // Match the given input against this pattern
            Matcher matcher = pattern.matcher(email);
            return matcher.matches();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private static boolean isValidPassword(String password) {
        return password.matches("[A-Za-z0-9]+") && hasValidLenghtPassword(password);
    }

    private static boolean hasValidLenghtPassword(String password) {
        return password.length() >= 8 && password.length() <= 10;
    }

    private static boolean isValidIdLength(String identifier) {
        return identifier.length() == 10;
    }

    private static boolean isValidRucLength(String ruc) {
        return ruc.length() == 13;
    }
}

