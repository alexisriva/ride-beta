package espol.ride.utils;

public class WSMedia {

    public static String URL = "http://ws.espol.edu.ec/saac/wsMedia.asmx";

    public static String NAMESPACE = "http://academico.espol.edu.ec/webservices/";
    public static String METHOD_NAME = "downloadFoto";
    public static String SOAP_ACTION = "http://academico.espol.edu.ec/webservices/downloadFoto";

    public static String PROPERTY0 = "name";
}
