package espol.ride.utils;

import org.kxml2.kdom.Element;
import org.kxml2.kdom.Node;

public class WSRide {

    public static String URL = "http://ws.espol.edu.ec/saac/wsRide.asmx";
    public static String NAMESPACE = "http://tempuri.org/";

    private static String AUTH_HEADER_NAME = "GTSIAuthSoapHeader";
    private static String AUTH_HEADER_USER = "usuario";
    private static String AUTH_HEADER_KEY = "key";


    public static Element buildAuthHeader() {
        Element GTSIAuthHeader = new Element();
        GTSIAuthHeader.setNamespace(NAMESPACE);
        GTSIAuthHeader.setName(AUTH_HEADER_NAME);

        Element username = GTSIAuthHeader.createElement(NAMESPACE, AUTH_HEADER_USER);
        username.addChild(Node.TEXT,"alrivade");
        GTSIAuthHeader.addChild(Node.ELEMENT, username);

        Element password = GTSIAuthHeader.createElement(NAMESPACE, AUTH_HEADER_KEY);
        password.addChild(Node.TEXT, "jmMH2VY17PHPmUq2w7E2o7W4I2O9d16u");
        GTSIAuthHeader.addChild(Node.ELEMENT, password);

        return GTSIAuthHeader;
    }

}
