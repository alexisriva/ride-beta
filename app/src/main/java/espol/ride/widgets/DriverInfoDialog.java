package espol.ride.widgets;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;

import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import espol.ride.R;
import espol.ride.activities.MainBaseActivity;
import espol.ride.databinding.DialogTripRegisterBinding;
import espol.ride.utils.APIRide;
import espol.ride.utils.SessionManager;
import espol.ride.utils.Utils;

public class DriverInfoDialog extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        final Activity activity = getActivity();
        final Context context = getContext();
        final SessionManager sessionManager = new SessionManager(context);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = activity.getLayoutInflater();

        DialogTripRegisterBinding binding = DialogTripRegisterBinding.inflate(inflater);
        binding.conductor.setText(getArguments().getString("NAME"));
        binding.dialogNumber.setText(String.format("%s de %s viajes",
                String.valueOf(getArguments().getInt("AVERAGE")),
                String.valueOf(getArguments().getInt("NUMBER")))
        );
        binding.stop.setText(getArguments().getString("STOP"));
        binding.numSeats.setText(getArguments().getString("SEATS"));
        binding.license.setText(getArguments().getString("LICENSE"));
        binding.carcolor.setBackgroundColor(Color.parseColor(getArguments().getString("COLOR")));

        builder.setView(binding.getRoot());

        builder.setMessage("Trip info")
            .setPositiveButton("Register", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(final DialogInterface dialog, int id) {
                    RequestParams params = new RequestParams();
                    params.put("trip", getArguments().getInt("TRIPID"));
                    params.put("stop", getArguments().getInt("STOPID"));
                    APIRide.post(APIRide.TRIPRECORD, params,
                            new Utils.CustomJsonHttpResponseHandler(context) {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            sessionManager.setRegistered(true);
                            Utils.shortToast(context, "You've been registered to the ride");

                            if (activity instanceof MainBaseActivity)
                                ((MainBaseActivity) activity).setPagerAdapter(true, true);
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            Utils.shortToast(context, "An error occurred, please try again later");
                            dialog.dismiss();
                        }
                    });
                }
            }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

        return builder.create();
    }
}
